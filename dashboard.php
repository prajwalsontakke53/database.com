 <center>
<?php

require("header.php");

authorization();

echo "Hello " . $_SESSION["username"];

?>
|
<a href="add_form.php">Add Record</a> |
<a href="logout.php">Logout</a>

<hr>

<?php
	if(isset($_REQUEST["msg"]) && $_REQUEST["msg"] != '') {
		echo $_REQUEST["msg"];
	}
?>


<?php

$sql = "SELECT * FROM users";

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
	
?>
<a href="exportcsv.php">Export Csv</a>
<table border="1">
	<thead>
		<th>Sr. No.</th>
		<th>Username</th>
		<th>Name</th>
		<th>Age</th>
		<th>City</th>
		<th>Status</th>
		<th>Action</th>
	</thead>
	<tbody>


<?php
    while($row = mysqli_fetch_assoc($result)) {
?>
		<tr>
			<td><?php echo $row["id"]; ?></td>
			<td><?php echo $row["username"]; ?></td>
			<td><?php echo $row["name"]; ?></td>
			<td><?php echo $row["age"]; ?></td>
			<td><?php echo $row["city"]; ?></td>
			<td><?php echo $row["status"]; ?></td>
			<td>
				<a href="edit_form.php?id=<?php echo $row["id"]; ?>">Edit</a>
				&nbsp;|&nbsp;
				<a href="delete.php?id=<?php echo $row["id"]; ?>" onclick="return confirm('Are you sure you want to delete this record?');">Delete</a></td>
		</tr>
<?php	
	}
?>

	</tbody>
</table>

<?php
} else {
	
	echo "No records found";
	
}
