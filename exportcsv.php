<?php

require("header.php");

authorization();

$filename = 'users.csv';


//$data = [
//			["name","age","city"],
//			["Shailesh", 31, "Mumbai"],
//			["Mahes", 27, "Nagpur"],
//];


$sql = "SELECT * FROM users";

$result = mysqli_query($conn, $sql);

// file creation
$file = fopen($filename, "w");

fputcsv($file, ["SrNo.", "username",	"password",	"display name",	"age",	"city",	"added date",	"updated date",	"status"]);


if (mysqli_num_rows($result) > 0) {
	
	while($row = mysqli_fetch_row($result)) {
		fputcsv($file, $row);
	}
}

fclose($file);

// download
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=".$filename);
header("Content-Type: application/csv; "); 

readfile($filename);

// deleting file
unlink($filename);
exit();